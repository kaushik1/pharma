import React, { Component } from "react"
import classNames from "classnames"
import { Link } from "react-router"
export default class Menu extends Component{
	onClickTab = (tabIndex) => {
		this.props.onChangeTabIndex(tabIndex)
	}
	render(){
		const {
			tabIndex,
			params,
		} = this.props
		return(
			<ul className="side-menu">
				<li className={classNames("treeview", {"active" : tabIndex === 1})}>
					<a href={`/user/record/${params.id}`}>
						<i className="fa fa-tag"></i>
						Record
					</a>
				</li>
				<li className={classNames("treeview", {"active" : tabIndex === 2})}>
					<a href={`/user/prescription/${params.id}`}>
						<i className="fa fa-tag"></i>
						Prescription
					</a>
				</li>
				<li className={classNames("treeview",{"active" : tabIndex === 3})}>
					<a href={`/user/approve/${params.id}`}>
						<i className="fa fa-tag"></i>
						Need Approval
					</a>
				</li>
			</ul>
		)
	}
}
