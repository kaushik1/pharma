import React, { Component } from 'react';
import "./header.css"
import Container from "../../containers/userProfile"

class Layout extends Component {
  render() {
    const { children, userProfile } = this.props;
    return (
      <div className='headerWrapper'>
        <div className="Logo">
        	PharmEasy
        </div>
        <div className="userDetail">
          Hi {userProfile.name}
        </div>
		  </div>
    )
  }
}

export default Container(Layout)
