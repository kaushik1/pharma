import React, { Component } from "react"
require("./modal.css")

class ConfirmationModal extends Component {
  onSuccessClick = () => {
    this.props.onConfirmation()
  }

  onCloseClick = () => {
    this.props.onCloseClick()
  }

  render() {
    const {
      message
    } = this.props
    return (
      <div className="modalContainer">
        <div className="modalHeader">
          Are you sure?
        </div>
        <div className="modalContent">
          {message}
        </div>
        <div className="modalFooter">
          <div className="buttonContainer">
            <button className="backButton" onClick={this.onCloseClick}>Go Back</button>
            <button className="proceedButton" onClick={this.onSuccessClick}>Proceed</button>
          </div>
        </div>
      </div>
    )
  }
}

export default ConfirmationModal
