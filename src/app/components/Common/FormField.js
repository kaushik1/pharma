import React,{ Component } from "react"

export const RenderTextField = (field) => (
    <div className="inputElement">
      <input {...field.input} type="text"/>
      {field.meta.touched && field.meta.error && 
       <span className="error">{field.meta.error}</span>}
    </div>
) 