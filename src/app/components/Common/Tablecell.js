import React, { Component } from "react"
import FixedDataTable from "fixed-data-table"
import ExampleImage from "./ExampleImageCell"
const {Table, Column, Cell} = FixedDataTable;

export const DateCell = ({rowIndex, data, col, ...props}) => (
  <Cell {...props}>
    {data[rowIndex][col].format('ddd, D MMM YYYY')}
  </Cell>
)

export const ImageCell = ({rowIndex, data, col, ...props}) => (
  <ExampleImage
    src={data[rowIndex][col]}
  />
)

export const LinkCell = ({rowIndex, data, col, ...props}) => (
  <Cell {...props}>
    <a href={data[rowIndex]["link"]} target="_blank">{data[rowIndex][col]}</a>
  </Cell>
)

export const TextCell = ({rowIndex, data, col, ...props}) => (
  <Cell {...props}>
    {data[rowIndex][col]}
  </Cell>
)

export const ApprovalButtonCell = ({
  rowIndex,
  data,
  col,
  action,
  onConfirmation,
  onCloseClick,
  ...props,
}) => {
  const status = data[rowIndex][col]
  const id = data[rowIndex]["id"]
  return (
    <Cell {...props}>
      {
        (status === 0 || status === -1) && (
          <button className="green button" onClick={() => {action(id, 1)}}>
            <i className="fa fa-check"/> Approve
          </button>
        )
      }
      {
        (status === 0 || status === 1) && (
          <button className="red button" onClick={() => {action(id, -1)}}>
            <i className="fa fa-close"/> Reject
          </button>
        )
      }
    </Cell>
  )
}

export const ButtonCell = ({
  rowIndex,
  data,
  col,
  action,
  onConfirmation,
  onCloseClick,
  ...props,
}) => {
  //const status = data[rowIndex][col]
  //const id = data[rowIndex]["id"]
  return (
    <Cell {...props}>
      {
        <button className="green button">
          <i className="fa fa-edit"/> Edit
        </button>
      }
      {
        <button className="red button">
          <i className="fa fa-close"/> Delete
        </button>
      }
    </Cell>
  )
}
