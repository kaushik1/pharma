import React, { Component } from 'react';
import { Link } from 'react-router';
import Header from "../Header"
import Body from "../Body"

import "./layout.css"
require("fixed-data-table/dist/fixed-data-table.css")

export default class Layout extends Component {
  constructor(props) {
    super(props)
    const { location } = this.props
    let index = -1
    const path = location.pathname
    if (path.includes("approve")) {
      index = 3
    } else if(path.includes("prescription")) {
      index = 2
    } else if(path.includes("record")){
      index = 1
    }
    this.state = {
      tabIndex:index,
    }
  }
  onChangeTabIndex = (tabIndex) => {
  	this.setState({
  		tabIndex,
  	})
  }

  render() {
    const { children, location, params } = this.props;
    const {
    	tabIndex,
    } = this.state
    return (
      <div className='layoutWrapper'>
        <Header />
        <Body
          tabIndex={tabIndex}
          children={children}
          params={params}
        />
      </div>
    );
  }
}
