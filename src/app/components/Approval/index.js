import React, { Component } from 'react'
import {
  TextCell,
  ImageCell,
  LinkCell,
  ApprovalButtonCell,
  DateCell
} from "../Common/Tablecell"
import {Table, Column, Cell} from 'fixed-data-table'
import ConfirmationModal from "../Common/ConfirmationModal"
import Container from "../../containers/approval"

class ApprovalList extends Component {
  state = {
    openModal: false,
    id: -1,
    status: -1,
  }

  onConfirmation = () => {
    const {
      changeApprovalStatus,
    } = this.props
    const {
      id,
      status,
    } = this.state
    this.setState({
      openModal: false,
    },() => {
      changeApprovalStatus(id, status);
    })
  }

  closeModal = ()=> {
    this.setState({
      openModal: false,
    })
  }

  openModal = (id, status) => {
    this.setState({
      openModal: true,
      id,
      status,
    })
  }
  render() {
    const {
      approvalList,
      changeApprovalStatus,
    } = this.props

    return (
      <div>
        <Table
          rowHeight={50}
          rowsCount={approvalList.length}
          width={1000}
          maxHeight={800}
          headerHeight={50}>
          <Column
            header={<Cell>Id</Cell>}
            cell={<TextCell data={approvalList} col="id" />}
            width={50}
          />
          <Column
            header={<Cell>Requested</Cell>}
            cell={<TextCell data={approvalList} col="request" />}
            flexGrow = {2}
            width={200}
          />
          <Column
            header={<Cell>Request By</Cell>}
            cell={<LinkCell data={approvalList} col="by" />}
            flexGrow = {1}
            width={200}
          />
          <Column
            header={<Cell>Requested On</Cell>}
            cell={<DateCell data={approvalList} col="when" />}
            flexGrow = {1}
            width={200}
          />
          <Column
            header={<Cell>Action</Cell>}
            cell={
              <ApprovalButtonCell
                data={approvalList}
                action={this.openModal}
                col="status"
              />
            }
            flexGrow = {2}
            width={200}
          />
        </Table>
        {
          this.state.openModal &&
          <div className="modalWrapper">
            <ConfirmationModal
              onConfirmation={this.onConfirmation}
              onCloseClick={this.closeModal}
              message={
                this.state.status === 1
                ? "You are about to share your information."
                : "We understand your privacy."
              }
            />
          </div>
        }
      </div>
    )
  }
}

export default Container(ApprovalList)
