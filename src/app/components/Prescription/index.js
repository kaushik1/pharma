import React, { Component } from 'react'
import {
  TextCell,
  ImageCell,
  LinkCell,
  ButtonCell,
  DateCell
} from "../Common/Tablecell"
import {Table, Column, Cell} from 'fixed-data-table'
import Container from "../../containers/prescription"

class PrescriptionList extends Component {
  render() {
    const {
      prescriptionList,
    } = this.props
    return (
      <div>
        <Table
          rowHeight={50}
          rowsCount={prescriptionList.length}
          width={1000}
          maxHeight={800}
          headerHeight={50}>
          <Column
            header={<Cell>Id</Cell>}
            cell={<TextCell data={prescriptionList} col="id" />}
            width={50}
          />
          <Column
            header={<Cell>Prescription By</Cell>}
            cell={<TextCell data={prescriptionList} col="by" />}
            flexGrow = {2}
            width={200}
          />
          <Column
            header={<Cell>Prescription Title</Cell>}
            cell={<TextCell data={prescriptionList} col="title" />}
            flexGrow = {1}
            width={200}
          />
          <Column
            header={<Cell>Prescription </Cell>}
            cell={<TextCell data={prescriptionList} col="prescription" />}
            flexGrow = {1}
            width={200}
          />
          <Column
            header={<Cell>Uploaded On</Cell>}
            cell={<DateCell data={prescriptionList} col="when" />}
            flexGrow = {1}
            width={200}
          />
        </Table>
      </div>
    )
  }
}

export default Container(PrescriptionList)
