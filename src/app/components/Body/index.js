import React, { Component } from 'react'
import classnames from "classnames"
import "./body.css"
import SideBar from "../SideBar/menu"

export default class Layout extends Component {
  render() {
    const { children, tabIndex, params } = this.props
    return (
      <div className="contentContainer">
        {
          tabIndex > 0 ? (
            <div className='sideBarWrapper'>
              <SideBar tabIndex={tabIndex} params={params}/>
            </div>
          ) : null
        }
        <div className={classnames({
            'bodyWrapper': tabIndex > 0,
            "wholeBodyWrapper": tabIndex < 0,
          })}>
          {children}
        </div>
      </div>
    );
  }
}
