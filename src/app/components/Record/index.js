import React, { Component } from 'react'
import {
  TextCell,
  ImageCell,
  LinkCell,
  ButtonCell,
  DateCell
} from "../Common/Tablecell"
import {Table, Column, Cell} from 'fixed-data-table'
import Container from "../../containers/record"

class RecordList extends Component {
  render() {
    const {
      recordList,
    } = this.props
    return (
      <div>
        <Table
          rowHeight={50}
          rowsCount={recordList.length}
          width={1000}
          maxHeight={800}
          headerHeight={50}>
          <Column
            header={<Cell>Id</Cell>}
            cell={<TextCell data={recordList} col="id" />}
            width={50}
          />
          <Column
            header={<Cell>Age</Cell>}
            cell={<TextCell data={recordList} col="age" />}
            flexGrow = {2}
            width={200}
          />
          <Column
            header={<Cell>BMI</Cell>}
            cell={<TextCell data={recordList} col="bmi" />}
            flexGrow = {1}
            width={200}
          />
          <Column
            header={<Cell>Vaccinated </Cell>}
            cell={<TextCell data={recordList} col="vaccine" />}
            flexGrow = {1}
            width={200}
          />
          <Column
            header={<Cell>Action</Cell>}
            cell={
              <ButtonCell
                data={recordList}
                action={this.openModal}
                col="status"
              />
            }
            flexGrow = {2}
            width={200}
          />
        </Table>
      </div>
    )
  }
}

export default Container(RecordList)
