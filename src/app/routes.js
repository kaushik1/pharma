import React from "react"
import { Route, IndexRoute, IndexRedirect, Redirect } from "react-router"
import Layout from './components/Layout/index'
import RecordList from "./components/Record"
import PrescriptionList from "./components/Prescription"
import ApprovalList from "./components/Approval"
import DoctorProfile from "./components/Profile/DoctorProfile"
import UserProfile from "./components/Profile/UserProfile"
import PharmacistProfile from "./components/Profile/PharmacistProfile"

export default (
  <Route component={Layout}>
    <Route path="/user/:id" component={UserProfile} />
    <Route path="/doctor/:id" component={DoctorProfile} />
    <Route path="/pharmacist/:id" component={PharmacistProfile} />
    <Route path="/user/record/:id" component={RecordList} />
    <Route path="/user/prescription/:id" component={PrescriptionList} />
    <Route path="/user/approve/:id" component={ApprovalList} />
  </Route>
)
