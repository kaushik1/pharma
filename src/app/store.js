import {createStore, applyMiddleware, combineReducers} from 'redux'
import  data from "./dataReducer"
import thunk from "redux-thunk"
import createLogger from "redux-logger"

const reducers = {
  data,     // <---- Mounted at 'form'
}

function configureStore(initialState) {
    const reducer = combineReducers(reducers)
    const store = createStore(
  		reducer,
      applyMiddleware(createLogger)
	  )
    return store;
}

let store = configureStore();

export default store;
