export const CHANGE_APPROVAL_STATUS = "CHANGE_APPROVAL_STATUS"

export function changeApprovalStatus(id, status) {
  return {
    type: CHANGE_APPROVAL_STATUS,
    id,
    status,
  }
}
