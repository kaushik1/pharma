import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import * as allAction from "../actions"

function mapStateToProps(state) {
  const {
    data,
  } = state
  return {
    prescriptionList: data.userPrescriptionList,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...allAction }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)
