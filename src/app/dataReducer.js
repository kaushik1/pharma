import moment from "moment"
import {
  CHANGE_APPROVAL_STATUS,
} from "./actions"
import findIndex from "lodash/findIndex"
import values from "lodash/values"
import clone from "lodash/clone"
const initialState = {
  userRecordList: [
    {
      id: 1,
      age: 25,
      bmi: 22.5,
      vaccine: 10,
    },
    {
      id:2,
      age:26,
      bmi:22.5,
      vaccine:8,
    },
  ],
  userPrescriptionList: [
    {
      id: 1,
      by: "Dr. Hrushab",
      when: moment(),
      title: "Viral test",
      prescription: "imageLink",
    },
    {
      id: 2,
      by: "Dr. Batra",
      when: moment(),
      title: "Hair Problem",
      prescription: "imageLink",
    },
  ],
  userNeedApprovalList: [
    {
      id: 1,
      request: "Prescription",
      by: "Dr. Hrushab",
      when: moment(),
      status: 0,
      link: "/doctor/118",
    },
    {
      id: 2,
      request: "Record",
      by: "Dr. Batra",
      when: moment().add(2, 'days'),
      status: -1,
      link: "/doctor/120",
    },
    {
      id: 3,
      request: "Prescription",
      by: "Wellness forever",
      when: moment().add(3, 'days'),
      status: 0,
      link: "/pharmacist/121",
    },
    {
      id: 4,
      request: "Prescription",
      by: "Wellness forever",
      when: moment().add(4, 'days'),
      status: 1,
      link: "/pharmacist/123",
    },
    {
      id: 5,
      request: "Record",
      by: "Dr. Shantu",
      when: moment().add(5, 'days'),
      status: 0,
      link: "/doctor/123",
    },
  ],
  userProfileDetail: {
    id: 123,
    name: "DUMMY NAME",
    number: "7259611206",
    email: "dummy@gmail.com"
  }
}


export default function data(state = initialState, action) {
  switch(action.type) {
    case CHANGE_APPROVAL_STATUS: {
      const {
        id,
        status,
      } = action
      const {
        userNeedApprovalList,
      } = state
      const copyApprovalList = userNeedApprovalList
      const matchIndex = findIndex(
        copyApprovalList, (approve) => {
          return approve.id === id
        }
      )
      if(matchIndex !== -1) {
        copyApprovalList[matchIndex]["status"] = status
      }
      return {
        ...state,
        userNeedApprovalList: clone(copyApprovalList),
      }
    }
    default:
      return initialState
  }
}
