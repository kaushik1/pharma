const path = require('path');
var BUILD_DIR = path.resolve(__dirname, 'public');
var APP_DIR = path.resolve(__dirname, 'src/app');

module.exports = {
  entry: APP_DIR + '/index.js',
  output: {
    path: BUILD_DIR,
    filename: 'bundle.js'
  },
  resolve: {
    extensions: ['.js', '.jsx', '.json','*'],
  },
  module: {
    loaders: [
    	{
        test: /\.(js|jsx)$/,
        loader: "babel-loader",
        query: {
          	cacheDirectory: true,
            plugins: ['transform-class-properties'],
        	},
      },
		  {
  		 	test: /\.css$/,
  		 	loader: ["style-loader","css-loader"]
		  },
	    {
        test: /\.json$/,
        loader: "json-loader",
    	},
    ]
  },
};
